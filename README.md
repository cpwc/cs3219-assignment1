# KWIC

CS3219 SOFTWARE ENGINEERING PRINCIPLES AND PATTERNS
Assignment 1

## Architectures Implementations

- Abstract Data Type
- Pipes and Filters

## Requirements

- Java SE Development Kit 8 / jdk8 and above

## Usage

#### File Input

    java <program name> <input file> <ignore file>