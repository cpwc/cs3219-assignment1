package pf;

import java.io.IOException;
import java.io.InputStream;

public class Input extends Filter {
	private InputStream in_;
	public Input(InputStream input, Pipe output) {
		super(null, output);
	    in_ = input;
	}

	@Override
	protected void transform() {
		 try{
		      boolean is_new_line = false;      
		      boolean is_new_word = false;
		      boolean is_line_started = false;
		      
		      int c = in_.read();
		      while(c != -1){
		        switch((byte) c){
		        case '\n':          
		          is_new_line = true;
		          break;
		        case ' ':
		          is_new_word = true;
		          break;
		        case '\t':
		          is_new_word = true;
		          break;
		        case '\r':
		          break;
		        default:
		          if(is_new_line){
		            output_.write('\n');
		            is_new_line = false;
		            is_line_started = false;
		          }
		          if(is_new_word){
		            if(is_line_started)
		              output_.write(' ');
		            is_new_word = false;
		          }
		          output_.write(c);
		          is_line_started = true;
		          break;
		        }        
		        c = in_.read();
		      }

		      output_.write('\n');
		      output_.closeWriter();
		    }catch(IOException exc){
		      exc.printStackTrace();
		      System.err.println("PipeAndFilter Error: Could not read the input file.");
		      System.exit(1);
		    }
	}


}
