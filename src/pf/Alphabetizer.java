package pf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.io.CharArrayWriter;
import java.io.IOException;

public class Alphabetizer extends Filter {

	public Alphabetizer(Pipe input, Pipe output) {
		super(input, output);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void transform() {
		try {
			ArrayList<String> lines = new ArrayList<String>();
			CharArrayWriter writer = new CharArrayWriter();

			int c = input_.read();
			while (c != -1) {
				writer.write(c);
				if (((char) c) == '\n') {
					String line = writer.toString().toLowerCase();
					String[] words = line.split("\\s");
					// filter shifted lines with illegal format
					if (!input_.ignoreStorage.getItems().contains(words[0])) {
						String newLine = "";
						for (int i = 0; i < words.length; i++) {
							if (input_.ignoreStorage.getItems().contains(words[i])) {
								newLine += (words[i] + " ");
							} else {
								newLine += (words[i].substring(0, 1).toUpperCase() + words[i].substring(1) + " ");
							}
						}
						newLine = (newLine.trim()+ "\n");
						lines.add(newLine);
					}
					writer.reset();
				}
				c = input_.read();
			}

			Collections.sort(lines);

			Iterator iterator = lines.iterator();
			while (iterator.hasNext()) {
				char[] chars = ((String) iterator.next()).toCharArray();
				for (int i = 0; i < chars.length; i++)
					output_.write(chars[i]);
			}
			output_.closeWriter();
		} catch (IOException exc) {
			exc.printStackTrace();
			System.err.println("PipeAndFilter Error: Could not sort circular shifts.");
			System.exit(1);
		}
	}


}
