package pf;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import pf.input.FileInput;
import pf.input.IInput;
import pf.input.Storage;

public class PipeAndFilter {
    public static void run(String inputFilename, String ignoreFilename) {
        try {

            FileInputStream in = new FileInputStream(inputFilename);
            IInput ignoreInput = new FileInput(new File(ignoreFilename));
            Storage ignoreStorage = ignoreInput.parse();

            Pipe in_cs = new Pipe();
            Pipe cs_al = new Pipe(ignoreStorage);
            Pipe al_ou = new Pipe();

            Input input = new Input(in, in_cs);
            CircularShifter shifter = new CircularShifter(in_cs, cs_al);
            Alphabetizer alpha = new Alphabetizer(cs_al, al_ou);
            Output output = new Output(al_ou, null);

            input.start(); // run it !
            shifter.start();
            alpha.start();
            output.start();
        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }
}
