package pf;

public abstract class Filter implements Runnable {
	 
	protected Pipe input_;
	protected Pipe output_;

	private boolean is_started_ = false;


	   public Filter(Pipe input, Pipe output){
	     input_ = input;
	     output_ = output;
	   }


	   //start thread
	   public void start(){
	     if(!is_started_){
	       is_started_ = true;
	       Thread thread = new Thread(this);
	       thread.start();
	     }
	   }

	   public void stop(){
	     is_started_ = false;
	   }

	   public void run(){
	     transform();
	   }

	   abstract protected void transform();
}
