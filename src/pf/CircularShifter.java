package pf;

import java.io.IOException;
import java.io.CharArrayWriter;
import java.util.StringTokenizer;

public class CircularShifter extends Filter {

	public CircularShifter(Pipe input, Pipe output) {
		super(input, output);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void transform() {
		try {
			CharArrayWriter writer = new CharArrayWriter();

			int c = input_.read();
			while (c != -1) {
				if (((char) c) == '\n') {
					String line = writer.toString();
					StringTokenizer tokenizer = new StringTokenizer(line);
					String[] words = new String[tokenizer.countTokens()];
					int i = 0;
					while (tokenizer.hasMoreTokens()) {
						words[i++] = tokenizer.nextToken();
					}

					for (i = 0; i < words.length; i++) {
						String shift = "";
						for (int j = i; j < (words.length + i); j++) {
							shift += words[j % words.length];
							if (j < (words.length + i - 1))
								shift += " ";
						}
						shift += '\n';
						char[] chars = shift.toCharArray();
						for (int j = 0; j < chars.length; j++)
							output_.write(chars[j]);
					}

					writer.reset();

				} else
					writer.write(c);

				c = input_.read();
			}

			output_.closeWriter();
		} catch (IOException exc) {
			exc.printStackTrace();
			System.err.println("PipeAndFilter Error: Could not make circular shifts.");
			System.exit(1);
		}

	}

}
