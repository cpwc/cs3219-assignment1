package pf;

import java.io.IOException;

public class Output extends Filter {

	public Output(Pipe input, Pipe output) {
		super(input, output);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void transform() {
		try {
			int c = input_.read();
			while (c != -1) {
				System.out.print((char) c);
				c = input_.read();
			}
		} catch (IOException exc) {
			exc.printStackTrace();
			System.err.println("PipeAndFilter Error: Broken pipe");
			System.exit(1);
		}
	}

}
