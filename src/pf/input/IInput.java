package pf.input;


/**
 * Created by WeiCheng on 8/30/2015.
 */
public interface IInput {

    Storage parse();
}