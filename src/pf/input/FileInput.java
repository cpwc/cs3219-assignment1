package pf.input;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


public class FileInput implements IInput {

    protected File file;

    public FileInput(File file) {
        this.file = file;
    }


    @Override
    public Storage parse() {

        Storage storage = new Storage();

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            String line;

            while ((line = br.readLine()) != null) {
                storage.addItem(line.trim());
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        }

        return storage;
    }
}
