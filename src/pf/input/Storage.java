package pf.input;

import java.util.ArrayList;
import java.util.List;

public class Storage {

    protected List<String> items = new ArrayList<String>();

    public void addItem(String item) {
        this.items.add(item);
    }

    public List<String> getItems() {
        return items;
    }

    public void setItems(List<String> items) {
        this.items = items;
    }

}