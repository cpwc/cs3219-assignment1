
package pf;

import java.io.PipedReader;
import java.io.PipedWriter;

import pf.input.Storage;

import java.io.IOException;

public class Pipe {

	// Pipe reader.
	private PipedReader reader_;

	// Pipe writer.
	private PipedWriter writer_;

	public Storage ignoreStorage;
	
	//constructor
	
	public Pipe(Storage ignoreStorage) throws IOException {
		this.ignoreStorage = ignoreStorage;
		writer_ = new PipedWriter();
		reader_ = new PipedReader();
		writer_.connect(reader_);
	}
	
	public Pipe() throws IOException {
		writer_ = new PipedWriter();
		reader_ = new PipedReader();
		writer_.connect(reader_);
	}

	public void write(int c) throws IOException {
		writer_.write(c);
	}

	public int read() throws IOException {
		return reader_.read();
	}

	public void closeWriter() throws IOException {
		writer_.flush();
		writer_.close();
	}

	public void closeReader() throws IOException {
		reader_.close();
	}
}
