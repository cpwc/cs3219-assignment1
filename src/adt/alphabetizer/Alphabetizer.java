/*
 * The MIT License (MIT)
 * Copyright (c) 2015 Poh Wei Cheng, A0111815R
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package adt.alphabetizer;

import adt.ignorefilter.IgnoreFilter;
import adt.storage.IStorage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Alphabetizer implements IAlphabetizer {

    protected List<String> alphabetized = new ArrayList<>();

    public void alphabetize(IgnoreFilter ignoreFilter, IStorage ignoreStorage) {

        for(String line : ignoreFilter) {
            String[] words = line.split("\\s");
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < words.length; i++) {
                String word = words[i].toLowerCase();

                boolean isIgnore = false;
                for (String ignoreWord : ignoreStorage) {
                    if (word.equalsIgnoreCase(ignoreWord)) {
                        isIgnore = true;
                        break;
                    }
                }

                if (isIgnore) {
                    sb.append(word);
                } else {
                    sb.append(word.substring(0, 1).toUpperCase() + word.substring(1));
                }

                sb.append(" ");
            }
            alphabetized.add(sb.toString().trim());
        }

        Collections.sort(alphabetized);
    }

    @Override
    public Iterator<String> iterator() {
        return Collections.unmodifiableList(alphabetized).iterator();
    }
}
