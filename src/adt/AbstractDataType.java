/*
 * The MIT License (MIT)
 * Copyright (c) 2015 Poh Wei Cheng, A0111815R
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package adt;

import adt.alphabetizer.Alphabetizer;
import adt.circularshifter.CircularShifter;
import adt.ignorefilter.IgnoreFilter;
import adt.input.FileInput;
import adt.input.IInput;
import adt.output.ConsoleOutput;
import adt.storage.IStorage;

import java.io.File;

public class AbstractDataType {

    public static void run(String inputFilename, String ignoreFilename) {
        IInput lineInput = new FileInput(new File(inputFilename));
        IStorage lineStorage = lineInput.parse();

        IInput ignoreInput = new FileInput(new File(ignoreFilename));
        IStorage ignoreStorage = ignoreInput.parse();

        CircularShifter circularShifter = new CircularShifter();
        circularShifter.shift(lineStorage);

        IgnoreFilter ignoreFilter = new IgnoreFilter();
        ignoreFilter.filter(circularShifter, ignoreStorage);

        Alphabetizer alphabetizer = new Alphabetizer();
        alphabetizer.alphabetize(ignoreFilter, ignoreStorage);

        ConsoleOutput consoleOutput = new ConsoleOutput();
        consoleOutput.output(alphabetizer);
    }
}

