/*
 * The MIT License (MIT)
 * Copyright (c) 2015 Poh Wei Cheng, A0111815R
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import adt.AbstractDataType;
import pf.PipeAndFilter;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        if (args == null || args.length == 0) {
            System.out.println("Usage: input.txt ignore.txt");
            System.exit(0);
        }

        String inputFilename = args[0];
        String ignoreFilename = args[1];

        String outputFilename;
        if (args.length == 3) {
            outputFilename = args[2];
        }

        Scanner sc = new Scanner(System.in);

        System.out.println("+-------------------------+");
        System.out.println("|   CS3219 Assignment 1   |");

        while (true) {
            System.out.println("+-------------------------+");
            System.out.println("|      Architectures      |");
            System.out.println("+-------------------------+");
            System.out.println("| 1. ADT                  |");
            System.out.println("| 2. PF                   |");
            System.out.println("|                         |");
            System.out.println("| 0. Exit                 |");
            System.out.println("+-------------------------+");

            System.out.print("Select option: ");
            int selection = sc.nextInt();

            if (selection != 0) {
                System.out.println();
                System.out.println("+-------------------------+");
                System.out.println("|         Results         |");
                System.out.println("+-------------------------+");
            }

            switch(selection) {
                case 0:
                    System.exit(0);
                    break;
                case 1:
                    AbstractDataType.run(inputFilename, ignoreFilename);
                    break;
                case 2:
                    PipeAndFilter.run(inputFilename, ignoreFilename);
                    try {
                        Thread.sleep(20);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    break;
            }

            System.out.println();
        }

    }
}
